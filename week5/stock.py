from flask import Flask, redirect, url_for, render_template, request
import requests
import pandas as pd
import matplotlib as plt

app = Flask(__name__)

class StockInfo:
    def __init__(self, ticker):
        self.ticker = ticker
        self.api_key = '5BF635F8CRSHGB3'
        self.symbol_url = 'https://www.alphavantage.co/query?function=OVERVIEW&symbol=' + ticker + '&apikey=' + self.api_key
        self.price_url = 'https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=' + ticker + '&apikey=' + self.api_key
        self.news_url = 'https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers=' + ticker + '&apikey=' + self.api_key
        self.plot_url = 'https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=' + ticker + '&apikey=' + self.api_key
        
    def get_symbol_data(self):
        symbol_r = requests.get(self.symbol_url)
        symbol_data = symbol_r.json()
        return symbol_data
    
    def get_price_data(self):
        price_r = requests.get(self.price_url)
        price_data = price_r.json()
        return price_data
    
    def get_news_data(self):
        news_r = requests.get(self.news_url)
        news_data = news_r.json()
        return news_data
    
    def get_plot_data(self):
        plot_r = requests.get(self.plot_url)
        plot_data = plot_r.json()
        plot_data = pd.DataFrame(plot_data['Time Series (Daily)'])
        plot_data = plot_data.transpose()
        plot_data = plot_data.drop(['1. open', '2. high', '3. low', '4. close',
        '6. volume', '7. dividend amount', '8. split coefficient'], axis = 1)
        plot_data = plot_data.rename(columns = {'5. adjusted close': 'AdjClose'})
        for i in range(0, len(plot_data)):
            plot_data.iloc[i, 0] = float(plot_data.iloc[i, 0])
        return plot_data

@app.route("/", methods = ['POST', 'GET'])
def main():
    if request.method == 'POST':
        user_input = request.form['nm']
        return redirect(url_for("query", ticker = user_input))
    else:
        return render_template("index.html")

@app.route("/<ticker>", methods = ['POST', 'GET'])
def query(ticker):
    stock_info = StockInfo(ticker)
    search_result = None
    if request.method == 'GET':
        symbol_data = stock_info.get_symbol_data()
        price_data = stock_info.get_price_data()
        news_data = stock_info.get_news_data()
        plot_data = stock_info.get_plot_data()
        return render_template("result.html", symbol = symbol_data, price = price_data, news = news_data, plot = plot_data)

    else: 
        user_input = request.form['nm']
        return redirect(url_for("query", ticker = user_input))

