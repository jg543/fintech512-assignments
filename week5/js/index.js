;
$(function() {
	"use strict";
	const apikey = "5BF635F8CRSHGB3A";
	let $stockSymbol = $("#stock-symbol");
	let loading = false;
	let loadCount = 0;
	let stocInfos = {};
	let StockInfo = function() {
		return {
			init() {
				cocoMessage.config({
					duration: 3000,
				});
				$("#search-btn").click(() => {
					let symbol = $stockSymbol.val();
					if (!symbol) {
						cocoMessage.error("Please enter stock symbol！");
						return;
					}
					if (loading) {
						cocoMessage.error("Loading, please wait!");
						return;
					}
					loading = true;
					loadCount = 0;
					stocInfos = {
						"Stock Symbol": symbol
					};
					cocoMessage.loading("loading...");
					axios.get(
						"https://www.alphavantage.co/query?function=GLOBAL_QUOTE&symbol=" +
						symbol + "&apikey=" + apikey
					).then((response) => {
						console.log(response);
						loadCount++;
						let datas2,
							arr2 = [],
							item2;
						if (200 == response.status) {
							datas2 = response.data["Global Quote"];
							console.log(datas2)
							if (datas2) {
								for (var key in datas2) {
									if (key.indexOf("previous close") > 0) {
										stocInfos["previous close"] = datas2[key];
									}
									if (key.indexOf("open") >= 0) {
										stocInfos["open"] = datas2[key];
									}
									if (key.indexOf("price") >= 0) {
										stocInfos["Current price"] = datas2[key];
									}
									if (key.indexOf("volume") >= 0) {
										stocInfos["Volume"] = datas2[key];
									}
								}
							}
						}
						if (4 == loadCount) {
							loading = false;
							cocoMessage.destroyAll();
						}
					});
					axios.get(
						"https://www.alphavantage.co/query?function=OVERVIEW&symbol=" +
						symbol + "&apikey=" +
						apikey
					).then((response) => {
						//console.log(response);
						loadCount++;
						let datas0;
						if (200 == response.status) {
							datas0 = response.data;
							if (datas0) {
								let infos = [];
								if (datas0["Description"] && datas0["Description"].indexOf(
										"(" + symbol + ")") > 0) {
									stocInfos["Company Name"] = datas0["Description"].split(
										"(" + symbol + ")")[0];
								}
								stocInfos["Sector"] = datas0["Sector"];
								stocInfos["Industry"] = datas0["Industry"];
								stocInfos["Market Capitalization"] = datas0[
									"MarketCapitalization"];
								stocInfos["Price to Earnings Ratio"] = datas0[
									"QuarterlyEarningsGrowthYOY"];
								stocInfos["Earnings per share"] = datas0[
									"DividendPerShare"];
								stocInfos["Dividend and dividend yield"] = datas0[
									"DividendYield"];
								stocInfos["Stock exchange"] = datas0["Exchange"];
								stocInfos["52 week high"] = datas0["52WeekHigh"];
								stocInfos["52 week low"] = datas0["52WeekLow"];
							}
						}
						if (4 == loadCount) {
							loading = false;
							cocoMessage.destroyAll();
						}
					});
					axios.get(
						"https://www.alphavantage.co/query?function=NEWS_SENTIMENT&tickers=" +
						symbol + "&apikey=" +
						apikey
					).then((response) => {
						//console.log(response);
						loadCount++;
						let datas4,
							news = [],
							item3, idx = 0;
						if (200 == response.status) {
							datas4 = response.data.feed;
							if (datas4 && datas4.length > 0) {
								for (var index in datas4) {
									news.push(
										'<li class="list-group-item"><a href="' +
										datas4[index].url + '" target="_blank">' +
										datas4[index].title + '</a></li>');
									idx++;
									if (idx >= 5) {
										break;
									}
								}
							}
						}
						$('#news-list').html(news.join(""));
						if (4 == loadCount) {
							loading = false;
							cocoMessage.destroyAll();
						}
					});
					let infos = [];
					let $time = setInterval(() => {
						if (4 == loadCount) {
							for (var name in stocInfos) {
								infos.push(
									'<li class="list-group-item"><label class="info-title">' +
									name + ':</label><div class="info-text">' +
									stocInfos[name] + '</div></li>');
							}
							$('#list-group').html(infos.join(""));
							clearInterval($time);
						}
					}, 1000);
					axios.get(
						"https://www.alphavantage.co/query?function=TIME_SERIES_DAILY_ADJUSTED&symbol=" +
						symbol + "&apikey=" +
						apikey
					).then((response) => {
						console.log(response)
						loadCount++;
						if (200 == response.status) {
							let dailys = response.data["Time Series (Daily)"];
							let times = [],
								datas = [];
							if (dailys) {
								for (var time in dailys) {
									times.push(time);
									datas.push(dailys[time]["4. close"]);
								}
							}
							var chartDom = document.getElementById('chart-box');
							var myChart = echarts.init(chartDom);
							var option;
							option = {
								xAxis: {
									type: 'category',
									data: times
								},
								yAxis: {
									type: 'value'
								},
								series: [{
									data: datas,
									type: 'line'
								}]
							};
							option && myChart.setOption(option);
						}
						if (4 == loadCount) {
							loading = false;
							cocoMessage.destroyAll();
						}
					});
				});
			}
		}
	}();
	StockInfo.init();
});
